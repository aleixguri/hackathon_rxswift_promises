//
//  ViewController.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import UIKit
import PromiseKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let viewModel: TransactionViewModel = TransactionViewModel()
    let disposeBag = DisposeBag()
    @IBOutlet weak var loadingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        createScreenReactive()
        
    }
    @IBAction func didPressStartTransactionButton() {
        viewModel.loadingComponent()

    }
    
    public func createScreenReactive() {
        
        viewModel
            .showTransactionScreen
            .asDriver(onErrorJustReturn: false)
            .distinctUntilChanged()
            .drive(onNext: { [weak self] value in
                guard let strongSelf = self,
                    value == true else {
                        return
                }
                strongSelf.loadingLabel.text = "Loading"
                strongSelf.processTransaction()
                strongSelf.activityIndicator.isHidden = false
                strongSelf.activityIndicator.startAnimating()

            })
            .disposed(by: disposeBag)
    }
    
    public func processTransaction() {
        self.loadingLabel.text = "executing transaction"
        
        let transaction = Transaction(subject: "Mock", amount: 20)
        
        viewModel
            .creatingTransaction(transaction: transaction)
            .asDriver(onErrorJustReturn: false)
            .distinctUntilChanged()
            .drive(onNext: { [weak self] value in
                guard let strongSelf = self,
                    value == true else {
                        return
                }
                strongSelf.loadingLabel.text = "transaction finished successfully"
                strongSelf.activityIndicator.isHidden = true
                strongSelf.activityIndicator.stopAnimating()
            })
            .disposed(by: disposeBag)
    }
    
}

