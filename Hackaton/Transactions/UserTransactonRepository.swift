//
//  USerTransactonRepository.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import Foundation
import PromiseKit

class UserTransactonRepository: TransactionRepository {
    
    func executeTransaction(transaction: Transaction) -> Promise<Bool> {
        return Promise<Bool> { seal in
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
                seal.fulfill(true)
                
            })
        }
    }
}
