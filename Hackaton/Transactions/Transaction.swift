//
//  Transaction.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import Foundation

public struct Transaction: Equatable, Codable {
    
    // MARK: - Properties
    public internal(set) var subject: String
    public internal(set) var amount: Double

    // MARK: - Methods
    public init(subject: String, amount: Double) {
      
      self.subject = subject
      self.amount = amount
    }
}
