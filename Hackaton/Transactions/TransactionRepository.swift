//
//  TransactionRepository.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import Foundation
import PromiseKit

protocol TransactionRepository {
    func executeTransaction(transaction: Transaction) -> Promise<Bool>
}
