//
//  TransactionViewModel.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import PromiseKit

public class TransactionViewModel: CreatingTransactionResponder {
    let userTransactionRepository: UserTransactonRepository = UserTransactonRepository()
    
    public var view: Observable<TransactionView> { return viewSubject.asObservable() }
    private let viewSubject = BehaviorSubject<TransactionView>(value: .connectingToComponent)
    private let showingTransactionScreenSubject = BehaviorSubject<Bool>(value: false)

    private let transactionCompletedSuccessfull = BehaviorSubject<Bool>(value: false)
    public var showTransactionScreen: Observable<Bool> {
        return showingTransactionScreenSubject.asObservable()
    }

    
    // MARK: - Methods
    public init() {
        
    }
    public func loadingComponent() {
        showingTransactionScreenSubject.onNext(true)
    }
    
    public func creatingTransaction(transaction: Transaction) -> Observable<Bool> {
      viewSubject.onNext(.creatingTransaction(transaction: transaction))
        userTransactionRepository.executeTransaction(transaction: transaction).done(evaluateTransaction(isSuccess:)).catch { (error) in
            // handle here any error
        }
        return transactionCompletedSuccessfull
        
    }
    
     func evaluateTransaction(isSuccess: Bool) {
        transactionCompletedSuccessfull.onNext(isSuccess)
    }
    
    public func onComplete() {
        viewSubject.onNext(.completeTransaction)
    }
    
}
