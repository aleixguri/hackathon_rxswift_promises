//
//  TransactionView.swift
//  Hackaton
//
//  Created by Aleix Guri on 01.10.19.
//  Copyright © 2019 Payworks GmbH. All rights reserved.
//

import Foundation

public enum TransactionView {
  
  case connectingToComponent
  case creatingTransaction(transaction: Transaction)
  case completeTransaction
}
